import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `proyecto-idea-element`
 * 
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class ProyectoIdeaElement extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Hello [[prop1]]!</h2>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'proyecto-idea-element',
      },
    };
  }
}

window.customElements.define('proyecto-idea-element', ProyectoIdeaElement);
